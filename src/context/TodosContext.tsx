import {createContext} from "react";
import {Todo} from "../types/Todo.ts";

const TodosInitialObject: TodosContext = {
    todos: [],
    addTodo: () => {},
    deleteTodo: () => {}
}

interface TodosContext {
    todos: Todo[] | null;
    addTodo: (todo: Todo) => void;
    deleteTodo: (id: number | string) => void;
}

export const TodosContext = createContext<TodosContext>(TodosInitialObject);
