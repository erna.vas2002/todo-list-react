import './App.scss'
import {TodoList} from "./pages/TodoList/TodoList.tsx";
import {useEffect, useState} from "react";
import {TodosContext} from "./context/TodosContext.tsx";
import {TodosApi} from "./api/Todos.ts";
import {Todo} from "./types/Todo.ts";

function App() {
    const [todos, setTodos] = useState<Todo[]>([])
    const [pagination] = useState<string>('10')


    const addTodo = (todo: Todo) => {
        setTodos((prev) => [...prev, todo])
    }

    const deleteTodo = (id: number | string) => {
        setTodos((prev) => prev.filter((todo) => todo.id !== id))
    }

    useEffect(() => {
        const getTodos = async () => {
                const result = await TodosApi.getTodos(pagination)
                setTodos(result.data)
        }
        getTodos()
    }, [])

    return (
        <div className='app-container'>
            <TodosContext.Provider value={{todos: todos, addTodo: addTodo, deleteTodo: deleteTodo}}>
                <TodoList/>
            </TodosContext.Provider>
        </div>
    )
}

export default App
