import axios, {AxiosResponse} from "axios";
import {urlTodo} from "../const/urls.ts";
import {Todo} from "../types/Todo.ts";

export const TodosApi = {
    getTodos: async (pagination: string): Promise<AxiosResponse<Todo[]>> => {
        return await axios.get<Todo[]>(`${urlTodo}${pagination}`)
    }
}

