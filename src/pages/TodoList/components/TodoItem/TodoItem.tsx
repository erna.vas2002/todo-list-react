import {useContext} from "react";
import {TodosContext} from "../../../../context/TodosContext.tsx";
import {Todo} from "../../../../types/Todo.ts";

type TodoOmit = Omit<Todo, "userId">;

export const TodoItem = ({title, id}: TodoOmit) => {
    const {deleteTodo} = useContext(TodosContext)
    return (
        <div className='todo-item'>
            <b>{id}.</b> {' '}
            {title}
            <button onClick={() => deleteTodo(id)}>delete</button>
        </div>
    )
}
