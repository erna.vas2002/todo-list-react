import {TodoItem} from "./components/TodoItem/TodoItem.tsx";
import {useContext, useEffect, useState} from "react";
import {TodosContext} from "../../context/TodosContext.tsx";
import {Todo} from "../../types/Todo.ts";
import { v4 as uuidv4 } from 'uuid';

const initialTodo: Todo = {
    id: 0,
    completed: false,
    title: '',
    userId: 0
}

export const TodoList = () => {
    const [inputValue, setInputValue] = useState<string>('')
    const {addTodo, todos} = useContext(TodosContext)


    useEffect(() => {
        const keyEnter = (event: any) => {
            if (event.key === 'Enter' && inputValue) {
                addTodo({...initialTodo, title: inputValue, id: uuidv4()})
                setInputValue('')
            }
        }

        document.addEventListener('keydown', keyEnter)

        return () => {
            document.removeEventListener('keydown', keyEnter)
        }
    }, [inputValue])

    const handleInputValue = (value: string) => {
        setInputValue(value)
    }

    const createTodo = () => {
        if (inputValue) {
            addTodo({...initialTodo, title: inputValue, id: uuidv4()})
            setInputValue('')
        }
    }

    return (
        <div className='todo-list-container'>
            <div>
                <input
                    placeholder='впишите туду'
                    onChange={(e) => handleInputValue(e.target.value)}
                    value={inputValue}
                />
            </div>
            {todos?.map((todo) => <TodoItem key={todo.id} title={todo.title} id={todo.id} completed={todo.completed}/>)}
            <button disabled={!inputValue} onClick={createTodo}>
                Добавить тудушку
            </button>
        </div>
    )
}
